package ba.unsa.etf.rma.spirala.budget;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import ba.unsa.etf.rma.spirala.R;
import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;
import ba.unsa.etf.rma.spirala.list.TransactionListFragment;
import ba.unsa.etf.rma.spirala.util.TransactionDBOpenHelper;

import static ba.unsa.etf.rma.spirala.list.TransactionListFragment.global;
import static ba.unsa.etf.rma.spirala.util.ConnectivityBroadcastReceiver.connectivity;

public class BudgetFragment extends Fragment implements IAccountView{
    View fragmentView;
    EditText totalL , monthL;
    TextView budgetT;
    public static TextView offlAcc;
    Button saveButton;
    private ITransactionBudgetPresenter accountPresenter;
    private Integer monthLimit,totalLimit;
    Account account=new Account();
    private ArrayList<Transaction> transaction;
    public static TransactionDBOpenHelper dbOpenHelper;

    public ITransactionBudgetPresenter getPresenter() {
        if (accountPresenter == null)
            accountPresenter = new TransactionBudgetPresenter(this,getActivity());

        return accountPresenter;
    }

    @Override
    public void setTransactions(ArrayList<Transaction> t) {
        this.transaction = t;
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void setAccount(Account accoun) {
        this.account = accoun;
        if (fragmentView != null) {
            budgetT.setText("Total budget: " + account.getBudget());
            totalL.setText(String.valueOf(account.getTotalLimit()));
            monthL.setText(String.valueOf(account.getMonthLimit()));

            global.setText("Global amount: " + account.getBudget());
            TransactionListFragment.limit.setText("Total Limit: " + account.getTotalLimit());
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_budget, container, false);

        dbOpenHelper = new TransactionDBOpenHelper(getActivity());
        getPresenter().getAccount();
        setElements();

        setBudget();
        return fragmentView;
    }

    private void setElements() {
        offlAcc = (TextView) fragmentView.findViewById(R.id.offlineAccount);
        totalL = (EditText) fragmentView.findViewById(R.id.total_limit);
        monthL = (EditText) fragmentView.findViewById(R.id.month_limit);
        budgetT = (TextView) fragmentView.findViewById(R.id.total_budget);
        saveButton = (Button) fragmentView.findViewById(R.id.saveLimit);
    }

    private void setBudget() {
        saveButton.setOnClickListener(new View.OnClickListener(){
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                okClick(v);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void okClick(View v) {
        if (String.valueOf(monthL.getText()).isEmpty() || String.valueOf(totalL.getText()).isEmpty() ) {
            Toast.makeText(getContext(), "Error, EMPTY EditText !", Toast.LENGTH_LONG).show();
            return;
        }
        monthLimit = Integer.valueOf(String.valueOf(monthL.getText()));
        totalLimit = Integer.valueOf(String.valueOf(totalL.getText()));

        if (monthLimit > account.getBudget() || totalLimit > account.getBudget()) {
            Toast.makeText(getContext(), "Error, Limit!", Toast.LENGTH_LONG).show();
            return;
        }

        // edit!!!!!
        Account acc = new Account(account.getBudget() ,totalLimit , monthLimit );
        if (!connectivity) {
            dbOpenHelper.addOneA(acc);
            Toast.makeText(getContext(), "Offline change of Limit!", Toast.LENGTH_LONG).show();
        } else {
            editAccToApi(acc);
        }

    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void editAccToApi(Account acc) {
        getPresenter().editAccount( String.valueOf(acc.getTotalLimit()) , String.valueOf(acc.getMonthLimit()));
        TransactionListFragment.limit.setText("Total Limit: " + acc.getTotalLimit());
        //Toast.makeText(getContext(), "You changed Limit!", Toast.LENGTH_LONG).show();
    }
}
