package ba.unsa.etf.rma.spirala.edit;

import android.widget.EditText;

import java.util.ArrayList;
import java.util.Date;

import ba.unsa.etf.rma.spirala.data.Transaction;

public interface ITransactionEditPresenter {
    ArrayList<String> setSpinnerArray(String title, String type);
    Integer getMonthLimit();
    Integer getTotalLimit();
    Integer getBudget();
    void setLimits(Integer tot , Integer mon);
    public void searchTransaction(String valueOf);
    public void deleteTransaction(String delete , String id);
    void editTransaction(String edit, String id, String title, String date, String amount,String itemDescription, String transactionInterval, String type, String endDate);
    Transaction getTransaction();
    void addTransaction(String edit, String title, String date, String valueOf, String itemDescription, String valueOf1, String type1, String endDate);
}
