package ba.unsa.etf.rma.spirala.budget;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;

public class TransactionAccountInteractor extends AsyncTask<String, Integer, Void> implements ITransactionAccountInteractor {

    String api_id = "f5e2f427-06e8-4484-ac7d-8a9bf17cc35b";
    Account account;
    public static ArrayList<Transaction> transactionArrayList;
    private OnAccountGetDone caller;

    public TransactionAccountInteractor(OnAccountGetDone p) {
        caller = p;
        account = new Account();
        transactionArrayList = new ArrayList<Transaction>();
    }


    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        caller.onDone(account, transactionArrayList);
    }

    public interface OnAccountGetDone {
        public void onDone(Account result, ArrayList<Transaction> transactions);
    }


    @Override
    protected Void doInBackground(String... strings) {
        String query = null;
        String query2 = null;
        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id;

        if (strings.length == 0) {
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String result = convertStreamToString(in);
                JSONObject jsonObject = new JSONObject(result);

                Integer id = jsonObject.getInt("id");
                Integer budget = jsonObject.getInt("budget");
                Integer totalLimit = jsonObject.getInt("totalLimit");
                Integer monthLimit = jsonObject.getInt("monthLimit");

                account = new Account(id, budget, totalLimit, monthLimit);
                getAllTransactions();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            query = strings[0];
            query2 = strings[1];
            String response = null;
            if (query == null || query == "null" || query2 == "null" || query2 == null) return null;

            JSONObject a = new JSONObject();
            try {
                a.put("totalLimit", Integer.valueOf(query));
                a.put("monthLimit", Integer.valueOf(query2));
                response =  getJson(url1 , a);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response.toString());

                Integer id = jsonObject.getInt("id");
                Integer budget = jsonObject.getInt("budget");
                Integer totalLimit = jsonObject.getInt("totalLimit");
                Integer monthLimit = jsonObject.getInt("monthLimit");
                account = new Account(id, budget, totalLimit, monthLimit);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public String getJson(String url,JSONObject params){
        InputStream is = null;
        try {
            URL _url = new URL(url);
            HttpURLConnection urlConn =(HttpURLConnection)_url.openConnection();
            urlConn.setRequestMethod("POST");
            urlConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            urlConn.setRequestProperty("Accept", "application/json");
            urlConn.setDoOutput(true);
            urlConn.connect();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(urlConn.getOutputStream()));
            writer.write(params.toString());
            writer.flush();
            writer.close();

            if(urlConn.getResponseCode() == HttpURLConnection.HTTP_OK){
                is = urlConn.getInputStream();// is is inputstream
            } else {
                is = urlConn.getErrorStream();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            response = sb.toString();
            Log.e("JSON", response);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        return response ;
    }

    protected Void getAllTransactions() {
        for (int j = 0; j < 7; j++) {
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions?page=" + j;


            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String result = convertStreamToString(in);
                JSONObject jo = new JSONObject(result);
                JSONArray results = jo.getJSONArray("transactions");
                for (int i = 0; i < results.length(); i++) {
                    JSONObject jsonObject = results.getJSONObject(i);
                    Integer id = jsonObject.getInt("id");
                    String title = jsonObject.getString("title");
                    Integer amount = jsonObject.getInt("amount");
                    Integer typeId = jsonObject.getInt("TransactionTypeId");
                    String itemDescription = jsonObject.getString("itemDescription");
                    String date = jsonObject.getString("date");
                    String endDate = jsonObject.getString("endDate");

                    Integer transactionInterval = null;
                    if (jsonObject.has("transactionInterval") && !jsonObject.isNull("transactionInterval")) {
                        transactionInterval = jsonObject.getInt("transactionInterval");
                    }
                    Date d1 = null;
                    Date d2 = null;
                    if (date != "null" &&  date != null) {
                        date = date.substring(0, 10);
                        @SuppressLint("SimpleDateFormat")
                        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                        try {
                            d1 = s.parse(date);
                            if (endDate != null && endDate != "null" && !endDate.isEmpty())
                                d2 = s.parse(endDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    String type = null;
                    switch (typeId) {
                        case 1:
                            type = "REGULARPAYMENT";
                            break;
                        case 2:
                            type = "REGULARINCOME";
                            break;
                        case 3:
                            type = "PURCHASE";
                            break;
                        case 4:
                            type = "INDIVIDUALINCOME";
                            break;
                        case 5:
                            type = "INDIVIDUALPAYMENT";
                            break;
                    }

                    Transaction tr = new Transaction(id, d1, amount, title, Transaction.type.valueOf(type), itemDescription, transactionInterval, d2);
                    //if ( !containsId(transactions , tr.getId()) )
                    transactionArrayList.add(tr);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
