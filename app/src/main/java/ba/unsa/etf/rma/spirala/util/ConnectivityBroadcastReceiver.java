package ba.unsa.etf.rma.spirala.util;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.RequiresApi;
import java.text.ParseException;
import java.util.ArrayList;
import ba.unsa.etf.rma.spirala.budget.BudgetFragment;
import ba.unsa.etf.rma.spirala.budget.TransactionAccountInteractor;
import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;
import ba.unsa.etf.rma.spirala.edit.TransactionEditFragment;
import ba.unsa.etf.rma.spirala.list.TransactionListFragment;
import static ba.unsa.etf.rma.spirala.budget.BudgetFragment.dbOpenHelper;
import static ba.unsa.etf.rma.spirala.budget.BudgetFragment.offlAcc;
import static ba.unsa.etf.rma.spirala.edit.TransactionEditFragment.offl;
import static ba.unsa.etf.rma.spirala.edit.TransactionEditFragment.openHelper;
import static ba.unsa.etf.rma.spirala.list.TransactionListFragment.offList;
import static ba.unsa.etf.rma.spirala.util.TransactionDBOpenHelper.flag;
import static ba.unsa.etf.rma.spirala.util.TransactionDBOpenHelper.flag2;

public class ConnectivityBroadcastReceiver extends BroadcastReceiver {
    public static boolean connectivity = true;

    @SuppressLint({"SetTextI18n", "UnsafeProtectedBroadcastReceiver"})
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() == null) {
            connectivity = false;
            Toast toast = Toast.makeText(context, "Disconnected", Toast.LENGTH_SHORT);
            toast.show();

            if (offList != null) offList.setText("Offline");
            if (offl != null) offl.setText("Offline");
            if (offlAcc != null) offlAcc.setText("Offline account edit");
        } else {
            connectivity = true;
            Toast toast = Toast.makeText(context, "Connected", Toast.LENGTH_SHORT);
            toast.show();
            if (offList != null) offList.setText(".");
            if (offl != null){
                offl.setText(".");
                TransactionListFragment fr = new TransactionListFragment();
                fr.takeFromWebApi();
            }
            if (offlAcc != null) offlAcc.setText(".");
            if (offlAcc != null && flag){
                if (dbOpenHelper == null) return;
                BudgetFragment budgetF = new BudgetFragment();
                try {
                    Account acc = dbOpenHelper.getAccFromSQL();
                    if (acc != null) budgetF.editAccToApi(acc);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (offl != null && flag2) {
                TransactionEditFragment editFragment = new TransactionEditFragment();
                ArrayList<Transaction> t = null;
                if (openHelper == null) return;
                try {
                    t = openHelper.getAllFromSQL("add");
                    if (t != null) {
                        for (int i = 0; i < t.size(); i++){
                            System.out.println(t.get(i));
                            if (i != 0 && t.get(i-1).getTitle() == t.get(i).getTitle())
                                continue;
                            editFragment.addTranToApi(t.get(i));
                        }
                    }
                    t = openHelper.getAllFromSQL("delete");
                    if (t != null) {
                        for (int i = 0; i < t.size(); i++){
                            System.out.println(t.get(i));
                            if (i != 0 && t.get(i-1).getTitle() == t.get(i).getTitle())
                                continue;
                            editFragment.deleteTranToApi(t.get(i));
                        }
                    }
                    t = openHelper.getAllFromSQL("edit");
                    if (t != null) {
                        for (int i = 0; i < t.size(); i++){
                            System.out.println(t.get(i));
                            if (i != 0 && t.get(i-1).getTitle() == t.get(i).getTitle())
                                continue;
                            editFragment.editTranOnApi(t.get(i));
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
