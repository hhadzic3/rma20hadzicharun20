package ba.unsa.etf.rma.spirala.budget;

import android.app.Activity;
import android.content.Context;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;

public class TransactionBudgetPresenter implements ITransactionBudgetPresenter , TransactionAccountInteractor.OnAccountGetDone{
    private IAccountView view;
    Context contextt;
    Activity activity;
    ArrayList<Transaction> transactions = new ArrayList<>();

    @Override
    public void getAccount(){
        new TransactionAccountInteractor((TransactionAccountInteractor.OnAccountGetDone) this).execute();
    }

    @Override
    public void editAccount(String totalLimit, String monthLimit) {
        new TransactionAccountInteractor((TransactionAccountInteractor.OnAccountGetDone) this).execute(totalLimit,monthLimit);
    }

    @Override
    public void onDone(Account result , ArrayList<Transaction> tr) {
        if (result != null)view.setAccount(result);
        view.setTransactions(tr);
        transactions = tr;
    }

    public TransactionBudgetPresenter(IAccountView viewContext, Context context) {
        view =  viewContext;
        this.contextt = context;
        activity = (Activity) context;
    }

    // STAROOOOOOOOOOOOOO za grafove


    // *** Months Charts ***
    @Override
    public Integer[] getNegMonths(ArrayList<Transaction> transaction) {
        transactions = transaction;
        return arrayNegatives(true);
    }

    Integer[] arrayNegatives(boolean neg){
        ArrayList<Transaction> negatives = getTransactions(neg);
        int januar = 0, februar = 0, mart = 0 ,april = 0, maj= 0 , juni= 0 ,  juli= 0 ,august = 0, sepetembar= 0
                , oktobar = 0, novembar = 0, decembar= 0;
        if (negatives.size() == 0) return new Integer[0];
        for (Transaction t : negatives) {
            if ( t.getDate() == null) continue;
            if (t.getDate().getMonth() == 0)
                januar += t.getAmount();
            if (t.getDate().getMonth() == 1)
                februar += t.getAmount();
            if (t.getDate().getMonth() == 2)
                mart += t.getAmount();
            if (t.getDate().getMonth() == 3)
                april += t.getAmount();
            if (t.getDate().getMonth() == 4)
                maj += t.getAmount();
            if (t.getDate().getMonth() == 5)
                juni += t.getAmount();
            if (t.getDate().getMonth() == 6)
                juli += t.getAmount();
            if (t.getDate().getMonth() == 7)
                august += t.getAmount();
            if (t.getDate().getMonth() == 8)
                sepetembar += t.getAmount();
            if (t.getDate().getMonth() == 9)
                oktobar += t.getAmount();
            if (t.getDate().getMonth() == 10)
                novembar += t.getAmount();
            if (t.getDate().getMonth() == 11)
                decembar += t.getAmount();

        }
        Integer[] array =  {januar , februar , mart , april , maj , juni , juli ,august , sepetembar , oktobar , novembar , decembar};
        return array;
    }

    @Override
    public Integer[] getPozMonths(ArrayList<Transaction> transaction) {
        transactions = transaction;
        return arrayNegatives(false);
    }

    @Override
    public Integer[] getTotMonths(ArrayList<Transaction> transaction) {
        transactions = transaction;
        if (transaction.size() == 0) return new Integer[0];
        Integer[] tot = {0 , 0 , 0, 0 ,0 ,0 ,0,0,0,0,0,0};

        Integer[] neg = arrayNegatives(true);
        Integer[] poz = arrayNegatives(false);

        for (int i = 0; i < 12; i++) {
            tot[i] = poz[i] - neg[i];
        }

        return tot;
    }

    ArrayList<Transaction> getTransactions(boolean neg){
        ArrayList<Transaction> negatives = new ArrayList<>();
        ArrayList<Transaction> pozitives = new ArrayList<>();
        for (Transaction t : transactions) {
            if (neg) {
                if (t.getTypeString().equals("INDIVIDUALPAYMENT") || t.getTypeString().equals("REGULARPAYMENT") || t.getTypeString().equals("PURCHASE"))
                    negatives.add(t);
            }
            else {
                if (t.getTypeString().equals("INDIVIDUALINCOME") || t.getTypeString().equals("REGULARINCOME"))
                    pozitives.add(t);
            }
        }
        if (neg) return negatives;
        return pozitives;
    }


    // ******* Weeksssssss ************

    @Override
    public String[] getWeeks() {
        return new String[] {"1" , "2" , "3" , "4"} ;
    }

    @Override
    public String[] getMonths() {
        return new String[] {"January", "February","March","April","May","June","Jule","August","Septembar","Ogust","November","December"};
    }

    @Override
    public String[] getDays() {
        return new String[] {"Monday", "Thusday","Wensday","Thursday","Friday","Saturday","Sunday"};
    }

    // *** Weeks Charts ***
    @Override
    public Integer[] getNegWeeks() {
        Integer[] neg = getNegativeWeeks(true);
        return neg;
    }

    @Override
    public Integer[] getPozWeeks() {
        Integer[] poz = getNegativeWeeks(false);
        return poz;
    }

    @Override
    public Integer[] getTotWeeks() {
        Integer[] neg = getNegativeWeeks(true);
        Integer[] poz = getNegativeWeeks(false);
        Integer[] tot = {0,0,0,0};

        for (int i = 0; i < 4; i++) {
            tot[i] = poz[i]-neg[i];
        }
        return tot;
    }

    private Integer[] getNegativeWeeks(boolean b) {
        ArrayList<Transaction> negatives = getTransactions(b);
        int first = 0, second = 0, third = 0 ,forth = 0;
        for (Transaction t : negatives) {
            if (t.getDate() == null) continue;
            Calendar cal = toCalendar(t.getDate());
            int w = cal.get(Calendar.WEEK_OF_MONTH);
            if (w == 1)
                first += t.getAmount();
            if (w == 2)
                second += t.getAmount();
            if (w == 3)
                third += t.getAmount();
            if (w == 4)
                forth += t.getAmount();
        }
        Integer[] arr = {first , second , third , forth};
        return arr;
    }
    private Calendar toCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
    // *** Weeks Charts ***



    // *** Days Charts ***
    @Override
    public Integer[] getNegDays() {
        Integer[] neg = getNegativeDays(true);

        return neg;
    }
    @Override
    public Integer[] getPozDays() {
        Integer[] poz = getNegativeWeeks(false);
        return poz;
    }


    @Override
    public Integer[] getTotDays() {
        Integer[] neg = getNegativeDays(true);
        Integer[] poz = getNegativeDays(false);
        Integer[] tot = {0,0,0,0 , 0 , 0 , 0};

        for (int i = 0; i < 7; i++) {
            tot[i] = poz[i]-neg[i];
        }
        return tot;

    }

    private Integer[] getNegativeDays(boolean b) {
        ArrayList<Transaction> negatives = getTransactions(b);
        int first = 0, second = 0, third = 0 ,forth = 0, fifth = 0 , sixth = 0, seventh = 0;
        for (Transaction t : negatives) {
            if (t.getDate() == null) continue;
            Calendar cal = toCalendar(t.getDate());
            int w = cal.get(Calendar.DAY_OF_WEEK);
            if (w == 1)
                first += t.getAmount();
            if (w == 2)
                second += t.getAmount();
            if (w == 3)
                third += t.getAmount();
            if (w == 4)
                forth += t.getAmount();
            if (w == 5)
                fifth += t.getAmount();
            if (w == 6)
                sixth += t.getAmount();
            if (w == 7)
                seventh += t.getAmount();
        }
        Integer[] arr = {first , second , third , forth , fifth , sixth , seventh};
        return arr;
    }



}
