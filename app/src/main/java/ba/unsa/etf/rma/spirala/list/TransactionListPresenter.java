package ba.unsa.etf.rma.spirala.list;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;
import static ba.unsa.etf.rma.spirala.list.TransactionListInteractor.transactionArray;

@RequiresApi(api = Build.VERSION_CODES.O)
public class TransactionListPresenter implements ITransactionListPresenter, TransactionListInteractor.OnTransactionsFilterDone{

    private ITransactionListView view;
    public Context context;
    public static ArrayList<Transaction> currentList;

    public TransactionListPresenter(ITransactionListView view, Context context) {
        this.view       = view;
        this.context    = context;
    }
    @Override
    public void refreshTransactions() {
        view.notifyTransactionListDataSetChanged();
    }


    @Override
    public void onDone(ArrayList<Transaction> results , Account result) {
        view.setTransactions(results);
        view.setAccount(result);
        view.notifyTransactionListDataSetChanged();
    }

    @Override
    public void getTransactionsFromWeb(String month, String year, String selectedItemFilter, String selectedItemSort){
        new TransactionListInteractor((TransactionListInteractor.OnTransactionsFilterDone) this).execute(month,year, selectedItemFilter , selectedItemSort);
    }

    public ArrayList<Transaction> setListByAll(int currentMonth, int currentYear){
        ArrayList<Transaction> tran = setListByMonth(currentMonth , currentYear);
        currentList = tran;
        return tran;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private ArrayList<Transaction> setListByMonth(int currentMonth , int curY) {
        ArrayList<Transaction> tran = new ArrayList<>();
        ArrayList<Transaction> sve = new ArrayList<>();
        if (currentList == null || currentList.isEmpty()) sve = transactionArray;
        else sve = currentList;
        for (Transaction t : sve) {
            // Potreban dobar format da bi se uzeo mijesec
            if (t == null || t.getDate() == null) continue;
            int mo = t.getDate().getMonth();
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(t.getDate());
            int year = calendar.get(Calendar.YEAR);
            //if (currentMonth-1 >= mo && year == curY && String.valueOf(t.getType()).startsWith("R") ){
              //  tran.add(t);
            //}
            if (currentMonth - 1==mo /*&& year == curY*/)
                tran.add(t);
        }
        return tran;
    }

}
