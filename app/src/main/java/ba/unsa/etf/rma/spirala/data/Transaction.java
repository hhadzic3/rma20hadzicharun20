package ba.unsa.etf.rma.spirala.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Transaction implements Parcelable {

    public Transaction() { }

    public enum type { INDIVIDUALPAYMENT, REGULARPAYMENT, PURCHASE, INDIVIDUALINCOME, REGULARINCOME };
    private Integer id;
    private Date date;
    private Integer amount;
    private String title; // duzi od 3 znaka i kraci od 15 znakova
    private String itemDescription; //null u slucajevima INCOME transakcija
    private Integer transactionInterval; //broj dana nakon cega se transakcija ponavlja, za REGULARINCOME i REGULARPAYMENT
    private Date endDate; //za REGULAR transakcije, predstavlja kraj pojavljivanja transakcije
    private type type;

    public Transaction(Date date, Integer amount, String title, type tip , String itemDescription, Integer transactionInterval, Date endDate) {
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = tip;
        this.itemDescription = itemDescription;
        this.transactionInterval = transactionInterval;
        this.endDate = endDate;
    }
    public Transaction(Integer id,Date date, Integer amount, String title, type tip , String itemDescription, Integer transactionInterval, Date endDate) {
        this.id = id;
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = tip;
        this.itemDescription = itemDescription;
        this.transactionInterval = transactionInterval;
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Transaction.type getType() {
        return type;
    }
    public String getTypeString() {
        return String.valueOf(type);
    }
    public void setType(Transaction.type type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public String getDateS() {
        return String.valueOf(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getTransactionInterval() {
        return transactionInterval;
    }

    public void setTransactionInterval(Integer transactionInterval) {
        this.transactionInterval = transactionInterval;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return  title ;
    }


    protected Transaction(Parcel in) {
        if (in.readByte() == 0) {
            amount = null;
        } else {
            amount = in.readInt();
        }
        title = in.readString();
        itemDescription = in.readString();
        if (in.readByte() == 0) {
            transactionInterval = null;
        } else {
            transactionInterval = in.readInt();
        }
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (amount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(amount);
        }
        dest.writeString(title);
        dest.writeString(itemDescription);
        if (transactionInterval == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(transactionInterval);
        }
    }

}
