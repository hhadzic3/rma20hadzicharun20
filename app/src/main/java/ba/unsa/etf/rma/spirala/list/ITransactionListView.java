package ba.unsa.etf.rma.spirala.list;

import java.util.ArrayList;

import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;

public interface ITransactionListView {

    void setTransactions(ArrayList<Transaction> movies);
    void notifyTransactionListDataSetChanged();

    void setAccount(Account result);
}
