package ba.unsa.etf.rma.spirala.list;

import java.util.ArrayList;

import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;

public interface ITransactionListPresenter {
    void refreshTransactions();
    //ArrayList<Transaction> setListByAll(int currentMonth, int currentYear, String selectedItemFilter, String selectedItemSort);
    //Integer getMonthLimit();
    //Integer getTotalLimit();
    //Integer getBudget();

    void onDone(ArrayList<Transaction> results , Account result);

    public void getTransactionsFromWeb(String s, String s1, String selectedItemFilter, String selectedItemSort);

    ArrayList<Transaction> setListByAll(int currentMonth, int currentYear);
}
