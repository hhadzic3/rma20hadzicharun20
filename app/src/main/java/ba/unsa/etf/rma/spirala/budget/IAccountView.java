package ba.unsa.etf.rma.spirala.budget;


import java.util.ArrayList;

import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;

public interface IAccountView {
    void setTransactions(ArrayList<Transaction> t);
    void setAccount(Account account);
}
