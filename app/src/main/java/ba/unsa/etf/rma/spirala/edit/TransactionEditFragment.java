package ba.unsa.etf.rma.spirala.edit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.basgeekball.awesomevalidation.AwesomeValidation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;
import java.util.TimeZone;

import ba.unsa.etf.rma.spirala.R;
import ba.unsa.etf.rma.spirala.budget.TransactionAccountInteractor;
import ba.unsa.etf.rma.spirala.budget.ViewPagerAdapter;
import ba.unsa.etf.rma.spirala.data.Transaction;
import ba.unsa.etf.rma.spirala.data.TransactionsModel;
import ba.unsa.etf.rma.spirala.list.MainActivity;
import ba.unsa.etf.rma.spirala.list.TransactionListAdapter;
import ba.unsa.etf.rma.spirala.list.TransactionListFragment;
import ba.unsa.etf.rma.spirala.list.TransactionListPresenter;
import ba.unsa.etf.rma.spirala.util.ConnectivityBroadcastReceiver;
import ba.unsa.etf.rma.spirala.util.TransactionDBOpenHelper;

import static ba.unsa.etf.rma.spirala.budget.BudgetFragment.offlAcc;
import static ba.unsa.etf.rma.spirala.list.MainActivity.list;
import static ba.unsa.etf.rma.spirala.list.MainActivity.viewPager;
import static ba.unsa.etf.rma.spirala.list.MainActivity.viewPagerAdapter;
import static ba.unsa.etf.rma.spirala.list.TransactionListFragment.offList;
import static ba.unsa.etf.rma.spirala.util.ConnectivityBroadcastReceiver.connectivity;
import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

public class TransactionEditFragment extends  Fragment implements  AdapterView.OnItemSelectedListener , ITransactionEditView {

    Transaction transaction = null;
    String title, itemDescription, type, date, endDate;
    public static TextView offl;
    Integer amount, transactionInterval;
    EditText titleT, dateT, amountT, itemDescriptionT, transactionIntervalT, endDateT;
    Spinner typeS;
    Button del, save;
    View view;
    public static TransactionDBOpenHelper openHelper;
    private ITransactionEditPresenter transactionListPresenter;

    public ITransactionEditPresenter getPresenter() {
        if (transactionListPresenter == null)
            transactionListPresenter = new TransactionEditPresenter(this, getActivity());

        return transactionListPresenter;
    }

    @Override
    public void refreshView() {
        transaction = getPresenter().getTransaction();
        setValues();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_edit, container, false);
        setGuiElements();
        transaction = null;
        if (getArguments() != null && getArguments().containsKey("transaction")) {
            transaction = getArguments().getParcelable("transaction");
            if (transaction != null) { // EDIT
                setValues();
                setListener();
                if (!connectivity) {
                    offl.setText("Offline edit");
                    offList.setText("Offline edit");
                }
            } else { // ADD
                del.setEnabled(false);
                title = " ";
                if (!connectivity) {
                    offl.setText("Offline add");
                    offList.setText("Offline add");
                }
                setSpinner(title);
                setAwesomeValidation(titleT, amountT, dateT);
            }
        } else {
            del.setEnabled(false);
            title = " ";
            if (!connectivity) {
                offl.setText("Offline add");
                offList.setText("Offline add");
            }
            setSpinner(title);
            setAwesomeValidation(titleT, amountT, dateT);
        }
        return view;
    }

    private void setGuiElements() {

        offl = (TextView) view.findViewById(R.id.offline);
        dateT = (EditText) view.findViewById(R.id.date);
        titleT = (EditText) view.findViewById(R.id.title);
        amountT = (EditText) view.findViewById(R.id.amount);
        itemDescriptionT = (EditText) view.findViewById(R.id.itemDescription);
        typeS = (Spinner) view.findViewById(R.id.type);
        transactionIntervalT = (EditText) view.findViewById(R.id.transactionInterval);
        endDateT = (EditText) view.findViewById(R.id.endDate);
        save = (Button) view.findViewById(R.id.save);
        del = (Button) view.findViewById(R.id.delete);

        save.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                try {
                    okClick(v);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        del.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                deleteClick(v);
            }
        });

        transactionIntervalT.setEnabled(false);
        endDateT.setEnabled(false);
        itemDescriptionT.setEnabled(false);
    }


    @SuppressLint("ResourceAsColor")
    private void setListener() {
        titleT.addTextChangedListener(new TextWatcher() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @SuppressLint("ResourceAsColor")
            public void afterTextChanged(Editable s) {
                titleT.setBackgroundColor(R.color.colorAccent);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        amountT.addTextChangedListener(new TextWatcher() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @SuppressLint("ResourceAsColor")
            public void afterTextChanged(Editable s) {
                amountT.setBackgroundColor(R.color.colorAccent);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        dateT.addTextChangedListener(new TextWatcher() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @SuppressLint("ResourceAsColor")
            public void afterTextChanged(Editable s) {
                dateT.setBackgroundColor(R.color.colorAccent);
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
        });
        typeS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                typeS.getBackground().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_ATOP);
                String item = parentView.getItemAtPosition(position).toString();
                setAfterClickOnSpinner(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
    }


    private void setValues() {
        type = transaction.getTypeString();
        titleT.setText(transaction.getTitle());
        amountT.setText(String.valueOf(transaction.getAmount()));
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat s = new SimpleDateFormat("dd.MM.yyyy");
        dateT.setText(s.format(transaction.getDate()));

        if (transaction.getItemDescription() != null)
            itemDescriptionT.setText(transaction.getItemDescription());

        if (transaction.getEndDate() != null)
            endDateT.setText(s.format(transaction.getEndDate()));

        if (transaction.getTransactionInterval() != null)
            transactionIntervalT.setText(String.valueOf(transaction.getTransactionInterval()));

        if (type.startsWith("R")) {
            endDateT.setEnabled(true);
            transactionIntervalT.setEnabled(true);
        }
        if (!type.endsWith("INCOME")) {
            itemDescriptionT.setEnabled(true);
        }
        setSpinner(transaction.getTitle());
    }


    public void setSpinner(String t) {
        ArrayList<String> categories = getPresenter().setSpinnerArray(t, type);
        typeS.setOnItemSelectedListener(this);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeS.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        setAfterClickOnSpinner(item);
    }

    public void setAfterClickOnSpinner(String item) {
        if (item.startsWith("R")) {
            endDateT.setEnabled(true);
            transactionIntervalT.setEnabled(true);
            if (transaction == null)
                setAwesomeValidation2(transactionIntervalT, endDateT);
        }
        if (!item.endsWith("INCOME")) {
            itemDescriptionT.setEnabled(true);
        }
        if (!item.startsWith("R")) {
            endDateT.setEnabled(false);
            transactionIntervalT.setEnabled(false);
            if (transaction == null)
                removeAwesomeValidation2(transactionIntervalT, endDateT);
        }
        if (item.endsWith("INCOME")) {
            itemDescriptionT.setEnabled(false);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void deleteClick(View view) {
        AlertDialog.Builder adb = new AlertDialog.Builder(view.getContext());
        adb.setTitle("Delete?");
        adb.setMessage("Are you sure you want to delete?");
        adb.setNegativeButton("Cancel", null);
        adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            public void onClick(DialogInterface dialog, int which) {
                int positionToRemove = TransactionListFragment.positionn;
                Transaction newT = new Transaction(transaction.getId() , transaction.getDate() ,transaction.getAmount() , transaction.getTitle(), transaction.getType() , transaction.getItemDescription() , transaction.getTransactionInterval() , transaction.getEndDate());
                boolean b = false;
                if (!connectivity) {
                    offl.setText("Offline delete");
                    offList.setText("Offline delete");

                    openHelper = new TransactionDBOpenHelper(getActivity());
                    b = openHelper.addOneT(newT, "delete");
                } else {
                    deleteTranToApi(newT);
                }
                if (!MainActivity.twoPaneMode) {
                    goBack();
                }
            }

        });
        adb.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void goBack() {
        list.remove(0);
        viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), list);
        viewPager.setAdapter(viewPagerAdapter);
        TransactionListFragment tr = new TransactionListFragment();
        tr.setTran();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void okClick(View view) throws ParseException {
        title = String.valueOf(titleT.getText());
        date = String.valueOf(dateT.getText());
        String amountS = String.valueOf(amountT.getText());
        if (amountS.matches("")) {
            Toast.makeText(view.getContext(), "Amount Error", Toast.LENGTH_LONG).show();
            return;
        }
        amount = Integer.valueOf(amountS);
        type = typeS.getSelectedItem().toString();
        if (type.startsWith("R")) {
            String transactionIntervalS = String.valueOf(transactionIntervalT.getText());
            if (transactionIntervalS != "" && transactionIntervalS != null )
                transactionInterval = Integer.valueOf(transactionIntervalS);
            endDate = String.valueOf(endDateT.getText());
        } else {
            transactionInterval = null;
            endDate = null;
        }
        if (!type.endsWith("INCOME"))
            itemDescription = String.valueOf(itemDescriptionT.getText());
        else itemDescription = null;
        //EDIT
        if (transaction != null) {
            if (endDate != null) {
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat s = new SimpleDateFormat("dd.MM.yyyy");
                Date d1 = s.parse(date);
                Date d2 = s.parse(endDate);
                if (d1.after(d2)) {
                    Toast.makeText(view.getContext(), "End Date Error", Toast.LENGTH_LONG).show();
                    return;
                }
            }
            AlertDialog.Builder adb = new AlertDialog.Builder(view.getContext());
            adb.setTitle("Edit?");
            if (amount > getPresenter().getMonthLimit())
                adb.setMessage("Are you sure? Your AMOUNT is ABOVE the Limit!");
            else adb.setMessage("Are you sure?");
            final int positionToRemove = TransactionListFragment.positionn;
            adb.setNegativeButton("Cancel", null);
            adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                public void onClick(DialogInterface dialog, int which) {
                    Transaction newT = getNewTransaction("edit");

                    if (!connectivity) {
                        openHelper = new TransactionDBOpenHelper(getActivity());
                        openHelper.addOneT(newT, "edit");
                    } else {
                        editTranOnApi(newT);
                    }
                    if (!MainActivity.twoPaneMode) {
                        goBack();
                    }
                }

            });
            adb.show();
        } else { // ADD
            if (valid()) {
                if (endDate != null) {
                    @SuppressLint("SimpleDateFormat")
                    SimpleDateFormat s = new SimpleDateFormat("dd.MM.yyyy");
                    Date d1 = s.parse(date);
                    Date d2 = s.parse(endDate);
                    if (d1.after(d2)) {
                        Toast.makeText(view.getContext(), "End Date Error", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                AlertDialog.Builder adb = new AlertDialog.Builder(view.getContext());
                adb.setTitle("Add?");
                if (amount > getPresenter().getMonthLimit())
                    adb.setMessage("Are you sure? Your AMOUNT is ABOVE the Limit!");
                else adb.setMessage("Are you sure?");
                adb.setNegativeButton("Cancel", null);
                adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                    @SuppressLint("ShowToast")
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    public void onClick(DialogInterface dialog, int which) {
                        Transaction newT = getNewTransaction("add");
                        boolean b = false;
                        if (!connectivity) {
                            openHelper = new TransactionDBOpenHelper(getActivity());
                            b = openHelper.addOneT(newT, "add");
                        } else {
                            addTranToApi(newT);
                        }
                        if (!MainActivity.twoPaneMode) {
                            list.remove(0);
                            viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), list);
                            viewPager.setAdapter(viewPagerAdapter);
                        }

                    }
                }).show();
            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void editTranOnApi(Transaction newT) {
        if (newT != null) {
            // pazi ... null ...  endDate
            String interval = "null";
            if (newT.getTransactionInterval() != null) {
                interval = String.valueOf(newT.getTransactionInterval());
            }
            getPresenter().editTransaction("edit", String.valueOf(newT.getId()), newT.getTitle(), String.valueOf(newT.getDate()), String.valueOf(newT.getAmount()), newT.getItemDescription(), interval, String.valueOf(newT.getType()), "null");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                TransactionListFragment.transactionListAdapter.notifyDataSetChanged();
                TransactionListFragment.listView.setAdapter(TransactionListFragment.transactionListAdapter);
            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void deleteTranToApi(Transaction transaction) {
        getPresenter().deleteTransaction("delete", String.valueOf(transaction.getId()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            TransactionListFragment.transactionListAdapter.notifyDataSetChanged();
            TransactionListFragment.listView.setAdapter(TransactionListFragment.transactionListAdapter);
        }
    }

    public void addTranToApi(Transaction newT) {
        if (newT != null) {
            // pazi ... null ...  endDate
            String interval = "null";
            if (newT.getTransactionInterval() != null) {
                interval = String.valueOf(newT.getTransactionInterval());
            }
            getPresenter().addTransaction("add", newT.getTitle(), String.valueOf(newT.getDate()), String.valueOf(newT.getAmount()), newT.getItemDescription(), interval, String.valueOf(newT.getType()), "null");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                TransactionListFragment.transactionListAdapter.notifyDataSetChanged();
                TransactionListFragment.listView.setAdapter(TransactionListFragment.transactionListAdapter);
            }
        }
    }

    public Transaction getNewTransaction(String reason) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat s = new SimpleDateFormat("dd.MM.yyyy");
        Transaction newT = null;
        try {
            if (endDate != null) {
                Date d1 = s.parse(date);
                Date d2 = s.parse(endDate);
                if (reason == "edit")
                    newT = new Transaction(transaction.getId(),d1, amount, title, Transaction.type.valueOf(type), itemDescription, transactionInterval, d2);
                else newT = new Transaction(d1, amount, title, Transaction.type.valueOf(type), itemDescription, transactionInterval, d2);

            } else {
                if (reason == "edit") newT = new Transaction(transaction.getId(), s.parse(date), amount, title, Transaction.type.valueOf(type), itemDescription, transactionInterval, null);
                else newT = new Transaction( s.parse(date), amount, title, Transaction.type.valueOf(type), itemDescription, transactionInterval, null);
            }
            } catch (ParseException e) {
            e.printStackTrace();
        }
        return newT;
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    private AwesomeValidation mAwesomeValidation;

    public void setAwesomeValidation(EditText titleT, EditText amountT, EditText dateT) {
        mAwesomeValidation = new AwesomeValidation(BASIC);
        mAwesomeValidation.addValidation(titleT, "^(?!.*  )(?=.*[\\w-])[\\w -]{3,15}$", "Enter valid title");
        mAwesomeValidation.addValidation(amountT, "^[-+]?\\d+$", "Enter valid amount");
        mAwesomeValidation.addValidation(dateT, "^\\s*(3[01]|[12][0-9]|0?[1-9])\\.(1[012]|0?[1-9])\\.((?:19|20)\\d{2})\\s*$", "Enter valid date");
    }

    public void setAwesomeValidation2(EditText transactionInterval, EditText endDate) {
        mAwesomeValidation.addValidation(transactionInterval, "^[-+]?\\d+$", "Enter valid transactionInterval");
        mAwesomeValidation.addValidation(endDate, "^\\s*(3[01]|[12][0-9]|0?[1-9])\\.(1[012]|0?[1-9])\\.((?:19|20)\\d{2})\\s*$", "error endDate");
    }

    public void removeAwesomeValidation2(EditText transactionInterval, EditText endDate) {
        mAwesomeValidation.addValidation(transactionInterval, ".*", "err_transactionInterval");
        mAwesomeValidation.addValidation(endDate, ".*", "err_endDate");
    }

    public boolean valid() {
        return mAwesomeValidation.validate();
    }
}
