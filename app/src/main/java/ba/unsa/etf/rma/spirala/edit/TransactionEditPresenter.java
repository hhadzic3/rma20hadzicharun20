package ba.unsa.etf.rma.spirala.edit;

import android.app.Activity;
import android.content.Context;
import java.util.ArrayList;
import java.util.Date;

import ba.unsa.etf.rma.spirala.data.AccountModel;
import ba.unsa.etf.rma.spirala.data.Transaction;

public class TransactionEditPresenter implements ITransactionEditPresenter,  TransactionEditInteractor.OnTransactionSearchDone{
    private Context contextt;
    private Activity activity;
    private Transaction transaction;
    private ITransactionEditView view;


    public TransactionEditPresenter(ITransactionEditView viewContext, Context context) {
        this.contextt = context;
        view = viewContext;
        activity = (Activity) context;
    }

    @Override
    public Transaction getTransaction() {
        return transaction;
    }

    @Override
    public void addTransaction(String edit, String title, String date, String valueOf, String itemDescription, String valueOf1, String type1, String endDate) {
        new TransactionEditInteractor((TransactionEditInteractor.OnTransactionSearchDone) this).execute("add",title,  date,  valueOf,  itemDescription,  valueOf1, type1,  endDate);
    }

    @Override
    public void searchTransaction(String valueOf) {
        new TransactionEditInteractor((TransactionEditInteractor.OnTransactionSearchDone) this).execute(valueOf);
    }
    @Override
    public void deleteTransaction(String delete , String id) {
        new TransactionEditInteractor((TransactionEditInteractor.OnTransactionSearchDone) this).execute(delete , id);
    }

    @Override
    public void editTransaction(String edit, String id, String title, String date, String amount, String itemDescription, String transactionInterval, String type, String endDate) {
        new TransactionEditInteractor((TransactionEditInteractor.OnTransactionSearchDone) this).execute(edit,id , title,  date,  amount,  itemDescription,  transactionInterval, type,  endDate);
    }

    @Override
    public void onDone(Transaction result, Boolean flag) {
        transaction = result;
        if (flag)
        view.refreshView();
    }


    @Override
    public ArrayList<String> setSpinnerArray(String title, String type) {
        ArrayList<String> categories = new ArrayList<>();
        if (title != " ") categories.add(type);
        categories.add("INDIVIDUALPAYMENT");categories.add("REGULARPAYMENT");
        categories.add("PURCHASE");categories.add("INDIVIDUALINCOME");categories.add("REGULARINCOME");

        return categories;
    }
    
    @Override
    public Integer getMonthLimit() {
        return AccountModel.account.getMonthLimit();
    }

    @Override
    public Integer getTotalLimit() {
        return AccountModel.account.getTotalLimit();
    }

    @Override
    public Integer getBudget() {
        return AccountModel.account.getBudget();
    }

    @Override
    public void setLimits(Integer tot, Integer mon) {
        AccountModel.account.setMonthLimit(mon);
        AccountModel.account.setTotalLimit(tot);
    }

}
