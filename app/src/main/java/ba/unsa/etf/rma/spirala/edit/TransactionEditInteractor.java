package ba.unsa.etf.rma.spirala.edit;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import androidx.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;
import ba.unsa.etf.rma.spirala.list.ITransactionListInteractor;

class TransactionEditInteractor extends AsyncTask<String, Integer, Void> implements ITransactionListInteractor {

    String api_id= "f5e2f427-06e8-4484-ac7d-8a9bf17cc35b";
    private TransactionEditInteractor.OnTransactionSearchDone caller;
    public Transaction transaction;
    private Boolean flag;
    private ArrayList<Transaction> transactions = new ArrayList<>();
    private Integer amountBudget = 0;

    public TransactionEditInteractor(TransactionEditInteractor.OnTransactionSearchDone p) {
        caller = p;
        transaction = new Transaction();
        flag = false;
    };

    public interface OnTransactionSearchDone{
        public void onDone(Transaction result ,Boolean flag);
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onDone(transaction ,flag);
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try { while ((line = reader.readLine()) != null) { sb.append(line + "\n"); }
        } catch (IOException e) {
        } finally { try {is.close(); } catch (IOException e) { } }
        return sb.toString();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected Void doInBackground(String... params) {
        String radnjaILIid = null;
        try {
            radnjaILIid = URLEncoder.encode(params[0], "utf-8");
        } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
        if (radnjaILIid == "delete") {
            String id2 = params[1];
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions/" + id2;
            String response =  deleteJson(url1);
            transaction = null;
        }
        else if (radnjaILIid == "edit"){
            String id = params[1];
            String title= params[2];
            String date= params[3];
            String amount= params[4];
            String itemDescription= params[5];
            String transactionInterval= params[6];
            String type= params[7];
            String endDate= params[8];

            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions/" + id;
            String response = null;
            JSONObject a = new JSONObject();
            try {
                a.put("title", title);
                a.put("date", date);
                a.put("amount", Integer.valueOf(amount));
                if (itemDescription != "null" && itemDescription != null)
                    a.put("itemDescription", itemDescription);
                if (transactionInterval != "null" && transactionInterval != null)
                    a.put("transactionInterval", Integer.valueOf(transactionInterval));
                if (endDate != "null" && endDate != null)
                    a.put("endDate", endDate);
                int typeId = 4;
                switch (type){
                    case "REGULARPAYMENT": typeId = 1;break;
                    case "REGULARINCOME": typeId =2;break;
                    case "PURCHASE": typeId = 3 ; break;
                    case "INDIVIDUALINCOME":typeId =4;break;
                    case "INDIVIDUALPAYMENT":typeId =5;break;
                }
                a.put("typeId", typeId);
                response =  getJson(url1 , a);
            } catch (JSONException e) { e.printStackTrace(); }
            JSONObject jsonObject = null;
            try {
                assert response != null;
                if (response != null)
                jsonObject = new JSONObject(response.toString());
                getResponse(jsonObject);
            } catch (JSONException e) { e.printStackTrace(); }

        }
        else if (radnjaILIid == "add"){
            String title= params[1];
            String date= params[2];
            String amount= params[3];
            String itemDescription= params[4];
            String transactionInterval= params[5];
            String type= params[6];
            String endDate= params[7];
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions";
            String response = null;
            JSONObject a = new JSONObject();
            try {
                a.put("title", title);
                a.put("date", date);
                a.put("amount", Integer.valueOf(amount));
                if (itemDescription != "null" && itemDescription != null)
                    a.put("itemDescription", itemDescription);
                if (transactionInterval != "null" && transactionInterval != null)
                    a.put("transactionInterval", Integer.valueOf(transactionInterval));
                if (endDate != "null" && endDate != null)
                    a.put("endDate", endDate);
                Integer typeId = null;
                switch (type){
                    case "REGULARPAYMENT": typeId = 1;break;
                    case "REGULARINCOME": typeId =2;break;
                    case "PURCHASE": typeId = 3 ; break;
                    case "INDIVIDUALINCOME":typeId =4;break;
                    case "INDIVIDUALPAYMENT":typeId =5;break;
                }
                a.put("typeId", typeId);
                Log.e("Provjeraaa:", a.toString());
                response =  getJson(url1 , a);
            } catch (JSONException e) { e.printStackTrace(); }
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response.toString());
                getResponse(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            flag = true;
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions/" + radnjaILIid;
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String result = convertStreamToString(in);
                JSONObject jsonObject = new JSONObject(result);

                getResponse(jsonObject);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        getAllTransactions();

        return null;
    }

    private void getResponse(JSONObject jsonObject) throws JSONException {
        Integer id1 = jsonObject.getInt("id");
        String title = jsonObject.getString("title");
        Integer amount = jsonObject.getInt("amount");
        String itemDescription = jsonObject.getString("itemDescription");
        String date = jsonObject.getString("date");
        String endDate = jsonObject.getString("endDate");
        int typeId = jsonObject.getInt("TransactionTypeId");

        Integer transactionInterval = null;
        if (jsonObject.has("transactionInterval") && !jsonObject.isNull("transactionInterval")) {
            transactionInterval = jsonObject.getInt("transactionInterval");
        }
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String dd = date.substring(0, 10);

        Date d1 = null;
        Date d2 = null;
        try {
            d1 = s.parse(dd);
            if (endDate != null && endDate != "null" && !endDate.isEmpty()) {
                String dd2 = endDate.substring(0, 10);
                d2 = s.parse(dd2);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String type = null;
        switch (typeId) {
            case 1:
                type = "REGULARPAYMENT";
                break;
            case 2:
                type = "REGULARINCOME";
                break;
            case 3:
                type = "PURCHASE";
                break;
            case 4:
                type = "INDIVIDUALINCOME";
                break;
            case 5:
                type = "INDIVIDUALPAYMENT";
                break;
        }
        Transaction tr = new Transaction(id1, d1, amount, title, Transaction.type.valueOf(type), itemDescription, transactionInterval, d2);
        transaction = tr;
    }


    public String deleteJson(String url){
        InputStream is = null;
        try {
            URL _url = new URL(url);
            HttpURLConnection urlConn =(HttpURLConnection)_url.openConnection();
            urlConn.setRequestMethod("DELETE");
            urlConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            urlConn.setRequestProperty("Accept", "application/json");
            urlConn.setDoOutput(true);
            urlConn.connect();

            if(urlConn.getResponseCode() == HttpURLConnection.HTTP_OK){
                is = urlConn.getInputStream();// is is inputstream
            } else {
                is = urlConn.getErrorStream();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            response = sb.toString();
            Log.e("JSON", response);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        return response ;
    }

    public String getJson(String url,JSONObject params){
        InputStream is = null;
        try {
            URL _url = new URL(url);
            HttpURLConnection urlConn =(HttpURLConnection)_url.openConnection();
            urlConn.setRequestMethod("POST");
            urlConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            urlConn.setRequestProperty("Accept", "application/json");
            urlConn.setDoOutput(true);
            urlConn.connect();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(urlConn.getOutputStream()));
            writer.write(params.toString());
            writer.flush();
            writer.close();

            if(urlConn.getResponseCode() == HttpURLConnection.HTTP_OK){
                is = urlConn.getInputStream();// is is inputstream
            } else {
                is = urlConn.getErrorStream();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            response = sb.toString();
            Log.e("JSON", response);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        return response ;
    }



    protected Void getAllTransactions() {
        Integer amountBudgetLocal = 0;
        for (int j = 0; j < 7; j++) {
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions?page=" + j;
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String result = convertStreamToString(in);
                JSONObject jo = new JSONObject(result);
                JSONArray results = jo.getJSONArray("transactions");
                for (int i = 0; i < results.length(); i++) {
                    JSONObject jsonObject = results.getJSONObject(i);
                    Integer amount = jsonObject.getInt("amount");
                    Integer typeId = jsonObject.getInt("TransactionTypeId");

                    if (typeId == 2 || typeId == 4) amountBudgetLocal += amount;
                }
                amountBudget = amountBudgetLocal;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        getNewAcc(String.valueOf(amountBudget));
        return null;
    }
    public void getNewAcc(String... strings){
        String query = strings[0];
        String response = null;
        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id;

        JSONObject a = new JSONObject();
        try {
            a.put("budget", Integer.valueOf(query));
            a.put("totalLimit", Integer.valueOf(query));
            response =  getJson(url1 , a);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



}
