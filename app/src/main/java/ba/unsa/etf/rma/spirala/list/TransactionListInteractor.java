package ba.unsa.etf.rma.spirala.list;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import androidx.annotation.RequiresApi;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.function.Predicate;

import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;


public class TransactionListInteractor extends AsyncTask<String, Integer, Void> implements ITransactionListInteractor {

    String api_id= "f5e2f427-06e8-4484-ac7d-8a9bf17cc35b";
    private OnTransactionsFilterDone caller;
    public static ArrayList<Transaction> transactions;
    Account account;
    public static ArrayList<Transaction> transactionArray;
    public Integer br = 0;

    public TransactionListInteractor(OnTransactionsFilterDone p) {
        caller = p;
        transactions = new ArrayList<Transaction>();
        transactionArray = new ArrayList<Transaction>();
        account = new Account();
        br = 0;
    };

    public interface OnTransactionsFilterDone{
        public void onDone(ArrayList<Transaction> results , Account result);
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onDone(transactions , account);
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected Void doInBackground(String... params) {

        String m = null;
        String y = null;
        String f = null;
        String so = null;

        try {
            m = URLEncoder.encode(params[0], "utf-8");
            y = URLEncoder.encode(params[1], "utf-8");
            f = URLEncoder.encode(params[2], "utf-8");
            so = URLEncoder.encode(params[3], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (br == 0) getAllTransactions();

        br++;

        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions"+
                "/filter?";

        assert so != null;
        String so2 = "title.asc";
        if (!so.equals("Sort+by")){
            if (so.equals("Title+-+Ascending")) so2 = "title.asc";

            if (so.equals("Price+-+Ascending")) so2 = "amount.asc";

            if (so.equals("Date+-+Ascending"))so2 = "date.asc";

            if (so == "Title+-+Descending")so2 = "title.desc";

            if (so == "Price+-+Descending")so2 = "amount.desc";

            if (so == "Date+-+Descending")so2 = "date.desc";

            url1 += "sort=" + so2;
        }

        assert f != null;
        if (!f.equals("Filter+by")) {
            Integer type = null;
            switch (f){
                case "REGULARPAYMENT": type = 1;break;
                case "REGULARINCOME": type =2;break;
                case "PURCHASE": type = 3 ; break;
                case "INDIVIDUALINCOME":type =4;break;
                case "INDIVIDUALPAYMENT":type =5;break;
            }
            url1 += "&typeId=" + type;
        }
        if (!so.equals("Sort+by") || !f.equals("Filter+by"))
            url1 += "&" + "month=" + m + "&" + "year=" + y;
        else url1 += "month=" + m + "&" + "year=" + y;

        Log.e("Provjera", url1);

        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(in);
            JSONObject jo = new JSONObject(result);
            JSONArray results = jo.getJSONArray("transactions");
            for (int i = 0; i < results.length(); i++) {
                JSONObject jsonObject = results.getJSONObject(i);
                Integer id = jsonObject.getInt("id");
                String title = jsonObject.getString("title");
                Integer amount = jsonObject.getInt("amount");
                Integer typeId = jsonObject.getInt("TransactionTypeId");
                String itemDescription = jsonObject.getString("itemDescription");
                String date = jsonObject.getString("date");
                String endDate = jsonObject.getString("endDate");

                Integer transactionInterval = null;
                if (jsonObject.has("transactionInterval") && !jsonObject.isNull("transactionInterval")) {
                    transactionInterval = jsonObject.getInt("transactionInterval");
                }
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                String dd = date.substring(0, 10);

                Date d1 = null;
                Date d2 = null;
                try {
                    d1 = s.parse(dd);
                    if (endDate != null && endDate != "null" && !endDate.isEmpty()) {
                        String dd2 = endDate.substring(0, 10);
                        d2 = s.parse(dd2);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String type = null;
                switch (typeId){
                    case 1:type ="REGULARPAYMENT";break;
                    case 2:type ="REGULARINCOME";break;
                    case 3:type ="PURCHASE";break;
                    case 4:type ="INDIVIDUALINCOME";break;
                    case 5:type ="INDIVIDUALPAYMENT";break;
                }

                Transaction tr = new Transaction(id,d1, amount, title, Transaction.type.valueOf(type), itemDescription, transactionInterval, d2);
                if ( !containsId(transactions , tr.getId()) )
                    transactions.add(tr);
            }
            getAcc();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean containsId(final ArrayList<Transaction> list, final int id){
        return list.stream().filter(new Predicate<Transaction>() {
            @Override
            public boolean test(Transaction o) {
                return o.getId().equals(id);
            }
        }).findFirst().isPresent();
    }

    protected Void getAcc() {
        String query = null;

        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id;

        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(in);
            JSONObject jsonObject = new JSONObject(result);

            Integer id = jsonObject.getInt("id");
            Integer budget = jsonObject.getInt("budget");
            Integer totalLimit = jsonObject.getInt("totalLimit");
            Integer monthLimit = jsonObject.getInt("monthLimit");

            account = new Account(id, budget, totalLimit, monthLimit);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected Void getAllTransactions() {
        for (int j = 0; j < 8; j++) {
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions?page=" + j;

            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String result = convertStreamToString(in);
                JSONObject jo = new JSONObject(result);
                JSONArray results = jo.getJSONArray("transactions");
                for (int i = 0; i < results.length(); i++) {
                    JSONObject jsonObject = results.getJSONObject(i);
                    Integer id = jsonObject.getInt("id");
                    String title = jsonObject.getString("title");
                    Integer amount = jsonObject.getInt("amount");
                    Integer typeId = jsonObject.getInt("TransactionTypeId");
                    String itemDescription = jsonObject.getString("itemDescription");
                    String date = jsonObject.getString("date");
                    String endDate = jsonObject.getString("endDate");

                    Integer transactionInterval = null;
                    if (jsonObject.has("transactionInterval") && !jsonObject.isNull("transactionInterval")) {
                        transactionInterval = jsonObject.getInt("transactionInterval");
                    }
                    Date d1 = null;
                    Date d2 = null;
                    if (date != "null" &&  date != null) {
                        date = date.substring(0, 10);
                        @SuppressLint("SimpleDateFormat")
                        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                        try {
                            d1 = s.parse(date);
                            if (endDate != null && endDate != "null" && !endDate.isEmpty())
                                d2 = s.parse(endDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    String type = null;
                    switch (typeId) {
                        case 1:
                            type = "REGULARPAYMENT";
                            break;
                        case 2:
                            type = "REGULARINCOME";
                            break;
                        case 3:
                            type = "PURCHASE";
                            break;
                        case 4:
                            type = "INDIVIDUALINCOME";
                            break;
                        case 5:
                            type = "INDIVIDUALPAYMENT";
                            break;
                    }

                    Transaction tr = new Transaction(id, d1, amount, title, Transaction.type.valueOf(type), itemDescription, transactionInterval, d2);

                    transactionArray.add(tr);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
