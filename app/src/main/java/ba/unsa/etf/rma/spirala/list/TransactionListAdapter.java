package ba.unsa.etf.rma.spirala.list;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import ba.unsa.etf.rma.spirala.R;
import ba.unsa.etf.rma.spirala.data.Transaction;

public class TransactionListAdapter extends ArrayAdapter<Transaction> {
    private int resource;
    public TextView titleV;
    public TextView amountV;
    public ImageView imageView;
    private ArrayList<Transaction> transactions;
    public TransactionListAdapter(@NonNull Context context, int _resource, ArrayList<Transaction> items) {
        super(context, _resource,items);
        resource = _resource;
    }

    public void setTransactions(ArrayList<Transaction> t) {
        transactions = t;
        this.addAll(transactions);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }
        final View view = View.inflate(getContext(), R.layout.list_element, null);
        view.setBackgroundColor(Color.BLUE);

        Transaction transaction = getItem(position);

        titleV = newView.findViewById(R.id.titleList);
        amountV = newView.findViewById(R.id.amountList);
        imageView = newView.findViewById(R.id.icon);

        titleV.setText(transaction.getTitle());
        amountV.setText( String.valueOf(transaction.getAmount()));

        String typeMatch = String.valueOf(transaction.getType()).toLowerCase();
        try {
            Class res = R.drawable.class;
            Field field = res.getField(typeMatch);
            int drawableId = field.getInt(null);
            imageView.setImageResource(drawableId);
        }
        catch (Exception e) {
            imageView.setImageResource(R.drawable.purchase);
        }
        return newView;
    }
}
