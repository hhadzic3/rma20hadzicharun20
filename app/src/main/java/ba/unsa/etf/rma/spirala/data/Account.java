package ba.unsa.etf.rma.spirala.data;

public class Account {

    private int id;
    private Integer budget;
    private Integer totalLimit;
    private Integer monthLimit;

    public Account(Integer budget, Integer totalLimit, Integer monthLimit) {
        this.budget = budget;
        this.totalLimit = totalLimit;
        this.monthLimit = monthLimit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account() {

    }

    public Account(Integer id, Integer budget, Integer totalLimit, Integer monthLimit) {
        this.id = id;
        this.budget = budget;
        this.totalLimit = totalLimit;
        this.monthLimit = monthLimit;
    }

    public Integer getBudget() {
        return budget;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }

    public Integer getTotalLimit() {
        return totalLimit;
    }

    public void setTotalLimit(Integer totalLimit) {
        this.totalLimit = totalLimit;
    }

    public Integer getMonthLimit() {
        return monthLimit;
    }

    public void setMonthLimit(Integer monthLimit) {
        this.monthLimit = monthLimit;
    }

    @Override
    public String toString() {
        return  budget + " " + totalLimit + " " + monthLimit ;
    }
}
