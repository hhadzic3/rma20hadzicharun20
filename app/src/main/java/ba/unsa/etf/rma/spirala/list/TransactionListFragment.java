package ba.unsa.etf.rma.spirala.list;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import java.time.LocalDate;
import ba.unsa.etf.rma.spirala.R;
import ba.unsa.etf.rma.spirala.budget.ITransactionBudgetPresenter;
import ba.unsa.etf.rma.spirala.budget.TransactionBudgetPresenter;
import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import android.os.Build;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static ba.unsa.etf.rma.spirala.list.TransactionListPresenter.currentList;
import static ba.unsa.etf.rma.spirala.util.ConnectivityBroadcastReceiver.connectivity;


@RequiresApi(api = Build.VERSION_CODES.O)
public class TransactionListFragment extends Fragment implements ITransactionListView ,AdapterView.OnItemSelectedListener{

    public static int positionn;
    private TransactionSpinnerAdapter transactionSpinnerAdapter;
    public static ListView listView;
    private Spinner sort , filter;
    private Button previous , next , add;
    public static TransactionListAdapter transactionListAdapter;
    private LocalDate today = LocalDate.now();
    public int currentMonth = today.getMonthValue();
    public int currentYear = 2020;
    public static TextView month, global,limit , offList;
    private String selectedItemFilter = "Filter by";
    private String selectedItemSort = "Sort by";
    private View fragmentView;
    View lastTouchedView;
    Account account=new Account();

    private ITransactionListPresenter transactionListPresenter;
    public ITransactionListPresenter getPresenter() {
        if (transactionListPresenter == null)
            transactionListPresenter = new TransactionListPresenter(this, getActivity());

        return transactionListPresenter;
    }

    @Override
    public void setTransactions(ArrayList<Transaction> t) {
        //transactionListAdapter.setTransactions(t);
        if (getContext() == null) return;
        if (t == null) return;
        transactionListAdapter = new TransactionListAdapter(Objects.requireNonNull(getContext()), R.layout.list_element,t);
        listView.setAdapter(transactionListAdapter);
    }

    @SuppressLint("SetTextI18n")
    public void setAccount(Account account) {
        this.account = account;

        global.setText("Global amount: " + account.getBudget());
        limit.setText("Total Limit: " + account.getTotalLimit());
    }


    @Override
    public void notifyTransactionListDataSetChanged() {
        transactionListAdapter.notifyDataSetChanged();
    }

    private void setTransactionList(){
        listView= (ListView) fragmentView.findViewById(R.id.listTransactions);
        transactionListAdapter = new TransactionListAdapter(fragmentView.getContext(), R.layout.list_element, new ArrayList<Transaction>());
        listView.setAdapter(transactionListAdapter);
    }

    private OnItemClick onItemClick;
    public interface OnItemClick {
        void onItemClicked(Transaction t);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_list, container, false);
        onItemClick = (OnItemClick) getActivity();
        setElementsOfFragment();

        setTransactionList();
        setSpinnerFilter();
        setSpinnerSort();
        setAmount();

        listView.setOnItemClickListener(listItemClickListener);

        takeFromWebApi();

        return fragmentView;
    }



    private void setElementsOfFragment() {
        offList = (TextView) fragmentView.findViewById(R.id.offlineList);
        month = (TextView) fragmentView.findViewById(R.id.month);
        next = (Button) fragmentView.findViewById(R.id.next);
        previous = (Button) fragmentView.findViewById(R.id.previous);
        add = (Button) fragmentView.findViewById(R.id.add);
        sort = (Spinner) fragmentView.findViewById(R.id.sort);
        filter = (Spinner) fragmentView.findViewById(R.id.filter);

        month.setText(getMonth(currentMonth) + ", " + currentYear );
        next.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                nextClick(v);
            }
        });
        previous.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                previousClick(v);
            }
        });
        add.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                addClick(v);
            }
        });
    }


    @SuppressLint("SetTextI18n")
    private void setAmount() {
        global = (TextView) fragmentView.findViewById(R.id.globalAmount);
        limit = (TextView) fragmentView.findViewById(R.id.limit);

        global.setText("Global amount: " + account.getBudget());
        limit.setText("Total Limit: " + account.getTotalLimit());
    }

    public void setSpinnerFilter(){
        ArrayList<String> categories = new ArrayList<>();
        categories.add("Filter by");categories.add("INDIVIDUALPAYMENT");categories.add("REGULARPAYMENT");
        categories.add("PURCHASE");categories.add("INDIVIDUALINCOME");categories.add("REGULARINCOME");
        categories.add("ALL TRANSACTIONS");
        int icons[] = { R.drawable.white ,R.drawable.individualpayment , R.drawable.regularpayment,R.drawable.purchase,R.drawable.individualincome,R.drawable.regularincome, R.drawable.white };

        filter.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

        transactionSpinnerAdapter = new TransactionSpinnerAdapter(fragmentView.getContext(), R.layout.spinner_element,icons , categories );
        filter.setAdapter(transactionSpinnerAdapter);
    }
    private void setSpinnerSort() {
        sort.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

        List<String> categories = new ArrayList<String>();
        categories.add("Sort by");
        categories.add("Price - Ascending");categories.add("Price - Descending");
        categories.add("Title - Ascending");categories.add("Title - Descending");
        categories.add("Date - Ascending");categories.add("Date - Descending");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(fragmentView.getContext(), android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sort.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        if (item.contains("-") && item != "Sort by" && item != "Filter by") selectedItemSort = item;

        else if (item != "Filter by" && item != "Sort by") selectedItemFilter = item;

        takeFromWebApi();
    }

    public void takeFromWebApi() {
        if (!connectivity) return;
        if (currentMonth < 10)
            getPresenter().getTransactionsFromWeb("0" + currentMonth , currentYear + "", selectedItemFilter, selectedItemSort);
        else getPresenter().getTransactionsFromWeb("" + currentMonth, currentYear + "", selectedItemFilter, selectedItemSort);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) { }

    public String getMonth(int month) {
        month = month -1;
        return new DateFormatSymbols().getMonths()[month];
    }

    @SuppressLint("SetTextI18n")
    public void previousClick(View view) {
        if (currentMonth == 1) {
            currentYear = currentYear - 1;
            currentMonth = 12;
        }
        else currentMonth = currentMonth -1;
        month.setText(getMonth(currentMonth) + ", " + currentYear);
        if (!connectivity) {
            setTran();
        }
        else takeFromWebApi();
    }

    @SuppressLint("SetTextI18n")
    public void nextClick(View view) {
        if (currentMonth == 12) {
            currentYear = currentYear + 1;
            currentMonth = 1;
        }
        else currentMonth = currentMonth +1;

        month.setText(getMonth(currentMonth) + ", " + currentYear );

        if (!connectivity) {
            setTran();
        }
        else takeFromWebApi();
    }

    public void setTran() {
        ArrayList<Transaction> t = getPresenter().setListByAll(currentMonth, currentYear);
        setTransactions(t);
    }

    public void addClick(View view) {
        onItemClick.onItemClicked(null);
        notifyTransactionListDataSetChanged();
    }

    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @SuppressLint("ResourceType")
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (lastTouchedView != null) lastTouchedView.setBackgroundColor(Color.WHITE);
            view.setBackgroundColor(R.color.design_default_color_primary);
            lastTouchedView = view;
            Transaction t;
            if (transactionListAdapter.getCount() != 0)
                t = transactionListAdapter.getItem(position);
            else return;
            positionn = position;
            onItemClick.onItemClicked(t);
        }
    };

}
