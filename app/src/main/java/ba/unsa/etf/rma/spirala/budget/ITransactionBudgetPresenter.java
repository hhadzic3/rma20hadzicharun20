package ba.unsa.etf.rma.spirala.budget;

import java.util.ArrayList;

import ba.unsa.etf.rma.spirala.data.Transaction;

public interface ITransactionBudgetPresenter {

    void getAccount();
    void editAccount(String s, String valueOf);

    String[] getWeeks();
    String[] getMonths();
    String[] getDays();

    Integer[] getNegWeeks();
    Integer[] getPozWeeks();
    Integer[] getTotWeeks();

    Integer[] getNegDays();
    Integer[] getPozDays();
    Integer[] getTotDays();

    Integer[] getNegMonths(ArrayList<Transaction> transaction);
    Integer[] getPozMonths(ArrayList<Transaction> transaction);
    Integer[] getTotMonths(ArrayList<Transaction> transaction);

}
