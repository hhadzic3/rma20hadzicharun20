package ba.unsa.etf.rma.spirala.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class TransactionsModel {

    public static ArrayList<Transaction> transactions = new ArrayList<Transaction>() {
        {
            SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy");
            try {
                add(new Transaction(date.parse("1.1.2020") , 1500 , "Salary" , Transaction.type.REGULARINCOME,null , 30 ,date.parse("1.1.2021")) );
                add(new Transaction(date.parse("2.1.2020") , 200 , "Upravni odbor" , Transaction.type.INDIVIDUALINCOME,null , null ,null));
                add(new Transaction(date.parse("3.1.2020") , 30 , "T-shert" , Transaction.type.PURCHASE,"Nike T-shert, very good" , null ,null));
                add(new Transaction(date.parse("4.1.2020") , 500 , "Bils" , Transaction.type.REGULARPAYMENT,"House evry month payment" , 30 ,date.parse("2.1.2021")));
                add(new Transaction(date.parse("5.1.2020") , 70 , "Protein" , Transaction.type.INDIVIDUALPAYMENT,"Use for growing muscles in gym" , null ,null));
                add(new Transaction(date.parse("1.2.2020" ), 1100 , "Salary Plus" , Transaction.type.REGULARINCOME,null , 30 ,date.parse("2.1.2021")));
                add(new Transaction(date.parse("2.2.2020") , 300 , "Sport add" , Transaction.type.INDIVIDUALINCOME,null , null ,null));
                add(new Transaction(date.parse("3.2.2020") , 40 , "Dux" , Transaction.type.PURCHASE,"Nike dux, very good" , null ,null));
                add(new Transaction(date.parse("5.2.2020") , 600 , "Plin" , Transaction.type.REGULARPAYMENT,"House evry month payment" , 30 ,date.parse("2.1.2021")));
                add(new Transaction(date.parse("6.2.2020") , 80 , "Meat" , Transaction.type.INDIVIDUALPAYMENT,"Use for growing muscles in gym" , null ,null));
                add(new Transaction(date.parse("7.3.2020") , 1700 , "Salary Winter plus" , Transaction.type.REGULARINCOME,null , 30 ,date.parse("2.1.2021")));
                add(new Transaction(date.parse("8.3.2020") , 300 , "Upravni odbor 2" , Transaction.type.INDIVIDUALINCOME,null , null ,null));
                add(new Transaction(date.parse("9.3.2020") , 40 , "Nike" , Transaction.type.PURCHASE,"Nike T-shert, very good" , null ,null));
                add(new Transaction(date.parse("10.3.2020") , 900 , "Water" , Transaction.type.REGULARPAYMENT,"House evry month payment" , 30 ,date.parse("2.1.2021")));
                add(new Transaction(date.parse("11.3.2020") , 10 , "Protein opet" , Transaction.type.INDIVIDUALPAYMENT,"Use for growing muscles in gym" , null ,null));
                add(new Transaction(date.parse("12.4.2020") , 2500 , "Salary Summer" , Transaction.type.REGULARINCOME,null , 30 ,date.parse("2.1.2021")));
                add(new Transaction(date.parse("13.4.2020") , 300 , "Upravni odbor Pivara" , Transaction.type.INDIVIDUALINCOME,null , null ,null));
                add(new Transaction(date.parse("4.4.2020") , 60 , "Adidas" , Transaction.type.PURCHASE,"Nike T-shert, very good" , null ,null));
                add(new Transaction(date.parse("6.4.2020") , 600 , "Electrisity" , Transaction.type.REGULARPAYMENT,"House evry month payment" , 30 ,date.parse("2.1.2021")));
                add(new Transaction(date.parse("7.4.2020") , 60 , "University pay" , Transaction.type.INDIVIDUALPAYMENT,"Use for growing muscles in gym" , null ,null));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };
}
