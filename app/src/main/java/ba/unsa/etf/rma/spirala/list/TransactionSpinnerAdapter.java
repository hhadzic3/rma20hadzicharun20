package ba.unsa.etf.rma.spirala.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

import ba.unsa.etf.rma.spirala.R;

public class TransactionSpinnerAdapter extends ArrayAdapter<String> {
    Context context;
    int icons[];
    ArrayList<String> types;
    public TextView names;
    public ImageView icon;

    public TransactionSpinnerAdapter(Context applicationContext, int res, int[] icons, ArrayList<String> typs) {
        super(applicationContext , res , typs);
        this.context = applicationContext;
        this.icons = icons;
        this.types = typs;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.spinner_element, parent, false);

        icon = (ImageView) row.findViewById(R.id.iconSpinner);
        names = (TextView) row.findViewById(R.id.typeSpinner);

        icon.setImageResource(icons[position]);
        names.setText(types.get(position));

        return row;
    }


}
