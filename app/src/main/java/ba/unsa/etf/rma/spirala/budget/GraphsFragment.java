package ba.unsa.etf.rma.spirala.budget;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import java.util.ArrayList;
import ba.unsa.etf.rma.spirala.R;
import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;

public class GraphsFragment extends Fragment implements AdapterView.OnItemSelectedListener, IAccountView {

    private View fragmentView;
    private Spinner timeSpinner;
    private BarChart BarChartNegativ , BarChartPozitiv , BarChartTotal;
    private ArrayList<String> labelsNames;
    private ArrayList<BarEntry> barEntryArrayList;

    private ITransactionBudgetPresenter transactionBudgetPresenter;
    private Account account;
    private ArrayList<Transaction> transaction;

    public ITransactionBudgetPresenter getPresenter() {
        if (transactionBudgetPresenter == null)
            transactionBudgetPresenter = new TransactionBudgetPresenter( this, getActivity());

        return transactionBudgetPresenter;
    }

    @Override
    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public void setTransactions(ArrayList<Transaction> t) {
        this.transaction = t;
        setChartForMonth(transaction);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_graph, container, false);
        getPresenter().getAccount();
        account = new Account();
        transaction = new ArrayList<Transaction>();
        setGuiElements();
        setSpinner();
        return fragmentView;
    }

    void setChartForMonth(ArrayList<Transaction> transaction){
        String[] Months = getPresenter().getMonths();

        setBarChart(BarChartNegativ , Months , getPresenter().getNegMonths(transaction));
        setBarChart(BarChartPozitiv , Months , getPresenter().getPozMonths(transaction));
        setBarChart(BarChartTotal , Months , getPresenter().getTotMonths(transaction));
    }

    void setGuiElements(){
        timeSpinner = (Spinner) fragmentView.findViewById(R.id.timeSpinner);
        BarChartNegativ = (BarChart) fragmentView.findViewById(R.id.negativ_graph);
        BarChartPozitiv = (BarChart) fragmentView.findViewById(R.id.pozitiv_graph);
        BarChartTotal = (BarChart) fragmentView.findViewById(R.id.total_graph);
    }

    private void setBarChart(BarChart barChart, String[] Months, Integer[] Negatives) {
        labelsNames = new ArrayList<>();
        barEntryArrayList = new ArrayList<>();

        for (int i = 0; i < Negatives.length; i++) {
            barEntryArrayList.add(new BarEntry(i, Negatives[i]));
            labelsNames.add(Months[i]);
        }

        BarDataSet barDataSet = new BarDataSet(barEntryArrayList, "Transactions");
        barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        BarData barData = new BarData(barDataSet);
        barChart.setData(barData);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(labelsNames));

        xAxis.setLabelCount(labelsNames.size());
        xAxis.setLabelRotationAngle(290);
        barChart.animateY(2000);
        barChart.invalidate();
    }

    public void setSpinner(){
        ArrayList<String> categories = setSpinnerArray();
        timeSpinner.setOnItemSelectedListener(this);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(fragmentView.getContext(), android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeSpinner.setAdapter(dataAdapter);
    }
    public ArrayList<String> setSpinnerArray() {
        ArrayList<String> categories = new ArrayList<>();
        categories.add("Month");categories.add("Week");categories.add("Day");
        return categories;
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        Toast.makeText(getContext() , item , Toast.LENGTH_LONG).show();
        if (item.equals("Month")){
            setChartForMonth(transaction);
        }
        if (item.equals("Week")){
            String[] weeks = getPresenter().getWeeks();
            setBarChart(BarChartNegativ , weeks , getPresenter().getNegWeeks());
            setBarChart(BarChartPozitiv , weeks , getPresenter().getPozWeeks());
            setBarChart(BarChartTotal , weeks , getPresenter().getTotWeeks());
        }
        if (item.equals("Day")){
            String[] days = getPresenter().getDays();
            setBarChart(BarChartNegativ , days , getPresenter().getNegDays());
            setBarChart(BarChartPozitiv , days , getPresenter().getPozDays());
            setBarChart(BarChartTotal , days , getPresenter().getTotDays());
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) { }
}
