package ba.unsa.etf.rma.spirala.util;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ba.unsa.etf.rma.spirala.data.Account;
import ba.unsa.etf.rma.spirala.data.Transaction;

public class TransactionDBOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "RMADataBase.db";
    public static final int DATABASE_VERSION = 1;


    public TransactionDBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public TransactionDBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static final String TRANSACTION_TABLE = "transactions";
    public static final String TRANSACTION_ID = "id";
    public static final String TRANSACTION_INTERNAL_ID = "internalId";
    public static final String TRANSACTION_TITLE = "title";
    public static final String TRANSACTION_DATE = "date";
    public static final String TRANSACTION_ENDDATE = "endDate";
    public static final String TRANSACTION_AMOUNT = "amount";
    public static final String TRANSACTION_ITEMDESCRIPTION = "itemDescription";
    public static final String TRANSACTION_INTERVAL = "transactionInterval";
    public static final String TRANSACTION_TYPE = "type";
    public static final String TRANSACTION_REASON = "reason";
    private static final String TRANSACTION_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TRANSACTION_TABLE + " ("  + TRANSACTION_INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + TRANSACTION_TITLE + " TEXT NOT NULL, "
                    + TRANSACTION_AMOUNT + " TEXT, "
                    + TRANSACTION_DATE + " TEXT, "
                    + TRANSACTION_TYPE + " TEXT , "
                    + TRANSACTION_ITEMDESCRIPTION + " TEXT, "
                    + TRANSACTION_INTERVAL + " TEXT, "
                    + TRANSACTION_ENDDATE + " TEXT, "
                    + TRANSACTION_REASON + " TEXT);";

    private static final String TRANSACTION_DROP = "DROP TABLE IF EXISTS " + TRANSACTION_TABLE;


    public static final String ACCOUNT_TABLE = "account";
    public static final String ACCOUNT_ID = "id";
    public static final String ACCOUNT_INTERNAL_ID = "internalId";
    public static final String ACCOUNT_BUDGET = "budget";
    public static final String ACCOUNT_TOTALLIMIT = "totalLimit";
    public static final String ACCOUNT_MONTHLIMIT = "monthLimit";
    private static final String ACCOUNT_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + ACCOUNT_TABLE + " ("  + ACCOUNT_INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + ACCOUNT_BUDGET + " TEXT NOT NULL, "
                    + ACCOUNT_TOTALLIMIT + " TEXT, "
                    + ACCOUNT_MONTHLIMIT + " TEXT); ";

    private static final String ACCOUNT_DROP = "DROP TABLE IF EXISTS " + ACCOUNT_TABLE;


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TRANSACTION_TABLE_CREATE);
        db.execSQL(ACCOUNT_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TRANSACTION_DROP);
        db.execSQL(ACCOUNT_DROP);
        onCreate(db);
    }
    public static boolean flag2 = false;
    public boolean addOneT(Transaction t , String reason){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        //za reset baze odkomentarisite liniju koda ispod kod
        //onUpgrade(db , 1, 2);
        flag2 = true;
        if (t == null) return false;
        if (reason == "delete" || reason == "edit")
            cv.put(TRANSACTION_INTERNAL_ID , t.getId());
        cv.put(TRANSACTION_TITLE , t.getTitle());
        cv.put(TRANSACTION_AMOUNT , t.getAmount());
        cv.put(TRANSACTION_DATE , t.getDateS());
        cv.put(TRANSACTION_TYPE , t.getTypeString());
        cv.put(TRANSACTION_ITEMDESCRIPTION , t.getItemDescription());
        cv.put(TRANSACTION_INTERVAL , t.getTransactionInterval());
        cv.put(TRANSACTION_ENDDATE , String.valueOf(t.getEndDate()));
        cv.put(TRANSACTION_REASON , reason);

        long insert = db.insert(TRANSACTION_TABLE , null , cv);
        return insert != -1;
    }

    public ArrayList<Transaction> getAllFromSQL(String reason) throws ParseException {
        ArrayList<Transaction> l = new ArrayList<>();

        String query = "SELECT * FROM " + TRANSACTION_TABLE + " WHERE " + TRANSACTION_REASON + " = \"" + reason + "\"";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query , null);
        if (cursor.moveToFirst()){
            do {
                int id = cursor.getInt(0);
                String title = cursor.getString(1);
                String amount = cursor.getString(2);
                String date = cursor.getString(3);
                String type = cursor.getString(4);
                String desc = cursor.getString(5);
                String interval = cursor.getString(6);
                String endDate = cursor.getString(7);

                Integer transactionInterval = null;
                if (interval != null) {
                    transactionInterval = Integer.valueOf(interval);
                }
                @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("EE MMM dd HH:mm:ss zz yyy");
                Date d = inputFormat.parse(date);
                @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("MMM dd, yyy h:mm a zz");
                Date d2 = null;
                System.out.println(id  + " " + d + " " + amount + " " + title+ " " + type + " " + desc + " " + interval + " " + endDate);

                Transaction transaction = new Transaction(id, d , Integer.valueOf(amount), title  , Transaction.type.valueOf(type) , desc , transactionInterval , d2 );
                l.add(transaction);
            }while (cursor.moveToNext());
            if (reason == "edit") onUpgrade(db , 1, 2);
        }
        else {if (reason == "edit") onUpgrade(db , 1, 2); }
        cursor.close();
        db.close();
        return l;
    }
    public static boolean flag = false;
    public boolean addOneA(Account a){
        flag = true;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        onUpgrade(db , 1, 2);
        cv.put(ACCOUNT_BUDGET , a.getBudget());
        cv.put(ACCOUNT_TOTALLIMIT , a.getTotalLimit());
        cv.put(ACCOUNT_MONTHLIMIT , a.getMonthLimit());

        long insert = db.insert(ACCOUNT_TABLE , null , cv);
        return insert != -1;
    }

    public Account getAccFromSQL() throws ParseException {
        Account a = new Account();
        String query = "SELECT * FROM " + ACCOUNT_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query , null);
        if (cursor.moveToFirst()){
            int id = cursor.getInt(0);
            String bud = cursor.getString(1);
            String tot = cursor.getString(2);
            String mon = cursor.getString(3);

            System.out.println(id  + " " + bud + " " + tot + " " + mon);

            a = new Account( id , Integer.valueOf(bud) , Integer.valueOf(tot) , Integer.valueOf(mon));
        }
        else { }
        cursor.close();
        db.close();
        return a;
    }


}
