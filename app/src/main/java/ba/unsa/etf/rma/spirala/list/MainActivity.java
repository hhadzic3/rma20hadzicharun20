package ba.unsa.etf.rma.spirala.list;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import android.annotation.SuppressLint;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.widget.FrameLayout;
import java.util.ArrayList;
import java.util.List;
import ba.unsa.etf.rma.spirala.R;
import ba.unsa.etf.rma.spirala.budget.BudgetFragment;
import ba.unsa.etf.rma.spirala.budget.GraphsFragment;
import ba.unsa.etf.rma.spirala.budget.ViewPagerAdapter;
import ba.unsa.etf.rma.spirala.data.Transaction;
import ba.unsa.etf.rma.spirala.edit.TransactionEditFragment;
import ba.unsa.etf.rma.spirala.util.ConnectivityBroadcastReceiver;

@RequiresApi(api = Build.VERSION_CODES.O)
@SuppressLint("StaticFieldLeak")
public class MainActivity extends AppCompatActivity  implements TransactionListFragment.OnItemClick{

    private int previousState, currentState;
    public static boolean twoPaneMode=false;
    public static ViewPager viewPager;
    public static ViewPagerAdapter viewPagerAdapter;
    public static List<Fragment> list;
    private ConnectivityBroadcastReceiver receiver = new ConnectivityBroadcastReceiver();
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FrameLayout details = findViewById(R.id.transaction_edit);
        //slucaj layouta za siroke ekrane
        if (details != null) {
            twoPaneMode = true;
            TransactionEditFragment detailFragment = (TransactionEditFragment) fragmentManager.findFragmentById(R.id.transaction_edit);
            if (detailFragment == null) {
                detailFragment = new TransactionEditFragment();
                fragmentManager.beginTransaction().replace(R.id.transaction_edit, detailFragment).commit();
            }
        }
        else twoPaneMode = false;

            fragmentManager.beginTransaction().replace(R.id.transaction_list, new TransactionListFragment()).commit();

            if (!twoPaneMode) {
            // SWIPE
            list = new ArrayList<>();
            list.add(new TransactionListFragment());
            list.add(new BudgetFragment());
            list.add(new GraphsFragment());

            viewPager = findViewById(R.id.pager);
            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), list);
            viewPager.setAdapter(viewPagerAdapter);
            viewPager.addOnPageChangeListener(onPageChangeListener);
            // SWIPE
        }
    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageSelected(int pageSelected) { }
        @Override
        public void onPageScrolled(int pageSelected, float positionOffset, int positionOffsetPixel) { }
        @Override
        public void onPageScrollStateChanged(int state) {
            int currentPage = viewPager.getCurrentItem();
            if (currentPage == list.size()-1 || currentPage == 0) {
                previousState = currentState;
                currentState = state;
                if (previousState == 1 && currentState == 0) {
                    viewPager.setCurrentItem(currentPage == 0 ? list.size()-1 : 0);
                }
            }
        }
    };

    @SuppressLint("ResourceAsColor")
    @Override
    public void onItemClicked(Transaction t) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("transaction", t);
        //if (t != null) arguments.putInt("id", t.getId());
        TransactionEditFragment detailFragment = new TransactionEditFragment();
        detailFragment.setArguments(arguments);
        if (twoPaneMode) {
            getSupportFragmentManager().beginTransaction().replace(R.id.transaction_edit, detailFragment).commit();
        }
        else {
            list.add(0 , detailFragment);
            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), list);
            viewPager.setAdapter(viewPagerAdapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }

}